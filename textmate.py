#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Change text to some other form of text.

Yes - this description is intentionally generic.

'''

# IMPORT STANDARD LIBRARIES
import textwrap


def convert_text(text, from_case='CapitalCase', to_case='camelCase'):
    '''Convert text from one known convention type to another.

    Args:
        text (str): The text to convert
        from_case (str): text's case
        to_case (str): The case to convert text towards

    '''
    if from_case == 'CapitalCase' and to_case == 'camelCase':
        return text[0].lower() + text[1:]

    return ''


def get_indent(text):
    '''Examine the given text and returns its initial indent.

    Args:
        text (str): The text to get the initial indent

    Returns:
        int : The initial indent, as spaces

    '''
    # TODO : Support tabs as the output, as well
    return len(text) - len(text.lstrip())


def indent(text, prefix, predicate=None):
    '''Indent the lines of text by some amount.

    This method is just a convenience method to implement textwrap.indent for
    all Python version < 3.3. Its interface/functionality is identical to
    the standard library function.

    Args:
        text (str): The text to indent.
        prefix (str): The string to indent each line in text.
        predicate (callable[str]):
            A function to determine whether to indent some function.
            If no function is given, a default function will be given which
            will indent every line.

    Returns:
        str: The indented text.

    '''
    try:
        function = textwrap.indent
    except AttributeError:
        def _indent(text, prefix, predicate=None):
            '''Replicate textwrap.indent, which is available in Python 3+.

            Args:
                text (str): The text to indent.
                prefix (str): The string to indent each line in text.
                predicate (callable[str]):
                    A function to determine whether to indent some function.
                    If no function is given, a default function will be given
                    which will indent every line.

            Returns:
                str: The indented text.

            '''
            def _should_indent(line):
                '''True: Always indent if there is text.'''
                return bool(line)

            if predicate is None:
                predicate = _should_indent

            return ''.join([prefix + line if predicate(line) else line
                            for line in text.splitlines(True)])
        function = _indent
    return function(text, prefix, predicate)

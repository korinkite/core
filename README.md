Personal repository of common helper modules for core Python tasks

This is an experimental branch where I've removed most of the core files in the
repository, leaving only the high-frequency, "most used" files.
